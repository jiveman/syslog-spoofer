#!/usr/bin/env python

# Script for spoofing and re-sending syslog messages.
# need to run as root
import socket
from socket import *
from scapy.all import IP,UDP,send
from ipaddress import ip_address

# Set dst_addr to an new address to override original or "" if you want the  packet to retain original destination addr
dst_addr = ""
listen_port = 2514
spoofed_src_addr = '10.0.3.4'

# Setup field that the source lies in
# sourcefield = 3

# Setup UDP socket listening on port 514
# sock = socket(AF_INET6, SOCK_DGRAM)
sock = socket(family=AF_INET, type=SOCK_DGRAM)
sock.bind(('', listen_port))

# to address updates on listen_port + 1
src_addr_sock = socket(family=AF_INET, type=SOCK_DGRAM)
src_addr_sock.bind(('', listen_port - 1))

# from address updates on listen_port + 1
dst_addr_sock = socket(family=AF_INET, type=SOCK_DGRAM)
dst_addr_sock.bind(('', listen_port + 1))

print("UDP Spoofer Listening on port {}".format(listen_port))
print("UDP Spoofer Listening for src addr updates on port {}".format(listen_port + 1))
print("UDP Spoofer Listening for dst addr updates on port {}".format(listen_port - 1))
# Setup program loop for reading data from socket and forwarding \
# to dst_addr spoofed as source as defined in message

while 1:
    # Read data from socket
    data, clientaddr = sock.recvfrom(4096)

    # Spoofed destination updates
    src_addr_update, src_addr_updateclient = src_addr_sock.recvfrom(1024)
    if src_addr_update:
        print("Spoof DEST IP Address update recv: {}".format(src_addr_update))
        try:
            new_ip = src_addr_update.decode('utf-8')
            if ip_address(new_ip):
                src_addr = new_ip
                print("[update] src: {}".format(src_addr))
        except Exception as err:
            print("{} is not a valid IP address for spoofing".format(new_ip))
            print(err)

    # Spoofed source updates
    dst_addr_update, dst_addr_updateclient = dst_addr_sock.recvfrom(1024)
    if dst_addr_update:
        print("Spoof SRC IP Address update recv: {}".format(dst_addr_update))
        try:
            new_ip = dst_addr_update.decode('utf-8')
            if ip_address(new_ip):
                dst_addr = new_ip
                print("[update] dest: {}".format(dst_addr))
        except Exception as err:
            print("{} is not a valid IP address for spoofing".format(new_ip))
            print(err)
    # IF you want to send as the machine sending,
    # Determine address to spoof based off of syslog message
    # spoofed_src_addr = gethostbyname("%s" %(data.split()[sourcefield]))
    # Try to send message back out with spoofed source in IP packet
    if not dst_addr:
        dst_addr = clientaddr[0]
    recv_port = clientaddr[1]
    print("Sent Data {} => Addr: '{}'".format("{}...".format(data), dst_addr))
    parts = data.decode('utf-8').split(' ')
    # pop() for removing [timeQuality tzKnown="1" isSynced="1" syncAccuracy="4827481"] from logline
    parts.pop(5)
    parts.pop(5)
    parts.pop(5)
    parts.pop(5)
    parts.pop(5)
    # print("re-joined data payload: {}".format(' '.join(parts)))
    data = ' '.join(parts).encode('utf-8')
    # for index, item in enumerate(parts):
    #     print(index, item)
    try:
        send(IP(src=src_addr, dst=dst_addr)/UDP(sport=recv_port, dport=514)/data.rstrip())
        print('sent packet to {} as {}'.format(dst_addr, src_addr))
        # send(IP(src=spoofed_src_addr, dst=dst_addr)/UDP(sport=42114, dport=514)/data.rstrip())
    except Exception as e:
        print(e)
        raise



