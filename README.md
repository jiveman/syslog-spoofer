# Syslog Spoofer

I wrote this utility to test emulating syslog traffic from various firewall devices.

* **syslog_spoofer** - is a relay service that spoofs src/dest ip addresses


Running syslog_spoofer usually requires root privileges. You'll also have to install the requirements.
```bash
python3 -m pip install scapy
```

### code example
Below is an example of how to update src / destination address after you've started up syslog_spoofer.

```python
def update_spoofed_address(addr, addr_type, port=2514):
    target_port = port
    if addr_type == 'src':
        target_port -= 1
    elif addr_type == 'dest':
        target_port += 1
    else:
        raise RuntimeError("addr_type must only be src or dest")

    logger.info("attempting to update {} spoofed address: {} sending to port: {}".format(addr_type, addr, target_port))
    bytesToSend         = str.encode(addr)
    serverAddressPort   = ("127.0.0.1", target_port)
    bufferSize          = 1024
    # Create a UDP socket at client side
    UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    # Send to server using created UDP socket
    UDPClientSocket.sendto(bytesToSend, serverAddressPort)

```

```python
update_spoofed_address(args.from_address, 'src')
update_spoofed_address(args.to_address, 'dest')

```

Testing out syslog traffic. Assuming your running syslog_spoofer on 2514, it stands up two additional sockets listening on 2513 & 2515 for src and dest IP address updates.

```bash
logger -P 2514 --udp --server 192.168.55.10 'Mom, Mama, Ma, Mommy'
```

If you're testing using `tcpdump` you may have to listen on loopback address (at least I did).

```bash
sudo tcpdump -i lo port 514
```

